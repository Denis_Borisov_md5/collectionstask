import java.util.*;

public class textForCollections
{
    public static void main(String[] args)
    {
        Map<String, Integer> unique = new LinkedHashMap<String, Integer>();

        for (String string : ("Britain is one of the most highly industrialised countries in the world: " +
                "for every person employed in agriculture 12 are employed in industry. " +
                "The original base of British industry was coal-mining, iron and steel and textiles. " +
                "Britain is one of the most highly industrialised countries in the world: " +
                "for every person employed in agriculture 12 are employed in industry. " +
                "The original base of British industry was coal-mining, iron and steel and textiles. " +
                "Britain is one of the most highly industrialised countries in the world: " +
                "for every person employed in agriculture 12 are employed in industry. " +
                "The original base of British industry was coal-mining, iron and steel and textiles. " +
                "Britain is one of the most highly industrialised countries in the world: " +
                "for every person employed in agriculture 12 are employed in industry. " +
                "The original base of British industry was coal-mining, iron and steel and textiles. " +
                "Britain is one of the most highly industrialised countries in the world: " +
                "for every person employed in agriculture 12 are employed in industry. " +
                "The original base of British industry was coal-mining, iron and steel and textiles." +
                "").split("\\p{Punct} | |\\p{Punct}"))
        {
            String [] arqw = string.split("\\p{Punct} | |\\p{Punct}");

            if (unique.get(string) == null)
                unique.put(string, 1);
            else
                unique.put(string, unique.get(string) + 1);
        }

        String uniqueString = join(unique.keySet(), ", ");

        List<Integer> value = new ArrayList<Integer>(unique.values());

        System.out.println("Unique words :" + uniqueString);
        System.out.println("Values of unique words :" + value);

        String [] arr = uniqueString.split(", ");
        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++)
            System.out.println("Sorted words by alphabet: " + arr[i]);
    }
    public static String join(Collection<String> s, String delimiter)
    {
        StringBuffer buffer = new StringBuffer();
        Iterator<String> iter = s.iterator();
        while (iter.hasNext())
        {
            buffer.append(iter.next());
            if (iter.hasNext())
            {
                buffer.append(delimiter);
            }
        }
        return buffer.toString();
    }
}
